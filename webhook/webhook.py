import json
import urllib.request
import configparser

WEBHOOK_SECTION = 'webhook'


def lambda_handler(event, context):
    # TODO implement
    print("event details - " + json.dumps(event))
    config = configparser.ConfigParser()
    config.read('../config.ini')

    for key in config.options(WEBHOOK_SECTION):
        print('value for key : {} ==> {}'.format(key, config.get(WEBHOOK_SECTION, key)))

    for key in config.options(WEBHOOK_SECTION):
        req = urllib.request.Request(config.get(WEBHOOK_SECTION, key), data={})
        result = urllib.request.urlopen(req)
        print('Response from Webhook : ' + str(result))

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
