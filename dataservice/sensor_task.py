from decimal import Decimal
import decimalencoder
import time
import logging
import json
import os
import boto3
from boto3.dynamodb.conditions import Key

# ---------------------------------------------------------------------------
# Configure log level for this lambda script
# Log level can be changed without deploying the application for debugging
# or different log level can be set different environment
#   LOG_LEVEL - passes as lambda environment variable
# ---------------------------------------------------------------------------
log = logging.getLogger()
log.setLevel(os.environ['LOG_LEVEL'])

# ---------------------------------------------------------------------------
# Configure dynamoDB boto3 resource for CRUD operation
#  TABLE_NAME environment variable is used to passed table name
#  in given environment. This will allow different table prefix
#  in different AWS environment
# ---------------------------------------------------------------------------
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['TABLE_NAME'])

# ---------------------------------------------------------------------------
# List of MSG used in this app
# ---------------------------------------------------------------------------
MANDATORY_FIELD_MISSING = 'Mandatory field missing in request'
REQ_NOT_SUPPORTED = 'Request not supported'

# ---------------------------------------------------------------------------
# List of column for field processing
# ---------------------------------------------------------------------------
_column_list = ['sensorID', 'timestamp', 'taskID']

# ---------------------------------------------------------------------------
# partition primary key for dynamoDB table
# ---------------------------------------------------------------------------
primary_key = 'sensorID'
sort_key = 'timestamp'


def lambda_handler(event, context):
    """
    This is the lambda handler method, this method will be invoked for every incoming request from AWS API Gateway.
     :param event:
     :param context:
     :return: the statusCode and response body
     """
    log.info('Request Payload: ' + str(event))
    request_operation = event['httpMethod']
    log.info('Http Request Method: ' + request_operation)

    if request_operation == 'GET':
        return handle_get_request(event)
    elif request_operation == 'POST':
        return handle_create_request(event)
    elif request_operation == 'PUT':
        return handle_update_request(event)
    elif request_operation == 'DELETE':
        return handle_delete_request(event)

    return bad_response(REQ_NOT_SUPPORTED)


def handle_update_request(event):
    """
    This method handles the update request for existing record.
        partition key   : sensorID
    :param event:
    :return: updated record
    """
    data = json.loads(event['body'])
    update_expression = "SET "
    expression_attribute_values = {}
    expression_attribute_names = {}

    if not required_field_present(data):
        return bad_response(MANDATORY_FIELD_MISSING)

    for field in _column_list:
        if (field in data) and field != primary_key and field != sort_key:
            if (type(data[field]) == float) or (type(data[field]) == int):
                expression_attribute_values[':{}'.format(field)] = Decimal(str(data[field]))
            else:
                expression_attribute_values[':{}'.format(field)] = data[field]
            expression_attribute_names['#{}'.format(field)] = field
            update_expression = update_expression + '#{} = :{}, '.format(field, field)

    log.info("update_expression : " + update_expression[:-2])
    log.info("expression_attribute_values : " + str(expression_attribute_values))

    result = table.update_item(
        Key={
            primary_key: data[primary_key],
            sort_key: data[sort_key]
        },
        ExpressionAttributeValues=expression_attribute_values,
        ExpressionAttributeNames=expression_attribute_names,
        UpdateExpression=update_expression[:-2],
        ReturnValues='ALL_NEW'
    )

    log.info("Attributes updated : " + json.dumps(result['Attributes'], cls=decimalencoder.DecimalEncoder))

    return {
        "statusCode": 200,
        "body": json.dumps(result['Attributes'], cls=decimalencoder.DecimalEncoder)
    }


def handle_create_request(event):
    """
    This method handles the create request for existing record.
        partition key   : sensorID
    :param event:
    :return: Record getting inserted
    """
    data = json.loads(event['body'])
    if 'timestamp' not in data:
        data['timestamp'] = str(time.time())
    if not required_field_present(data):
        return bad_response(MANDATORY_FIELD_MISSING)

    for field in _column_list:
        if (field in data) and ((type(data[field]) == float) or (type(data[field]) == int)):
            data[field] = Decimal(str(data[field]))

    log.info('Item getting inserted is : ' + str(data))
    table.put_item(Item=data)

    return {
        "statusCode": 201,
        "body": json.dumps(data, cls=decimalencoder.DecimalEncoder)
    }


def handle_get_request(event):
    """
    This method handles the update request for existing record.
        partition key   : sensorID
        sort key        : timestamp
    Without the sort key get_item can not work, hence using query approach
    to retrieve all the records matching siteID. Further this method can be
    extended to filter the result if the request contains siteID
    :param event:
    :return: all records with matching siteID
    """
    result = table.query(
        KeyConditionExpression=Key(primary_key).eq(event['queryStringParameters'][primary_key])
    )
    log.info("Records fetched from table : " + json.dumps(result['Items'], cls=decimalencoder.DecimalEncoder))
    return {
        "statusCode": 200,
        "body": json.dumps(result['Items'], cls=decimalencoder.DecimalEncoder)
    }


def handle_delete_request(event):
    """
    This method handles the update request for existing record.
        partition key   : sensorID
        sort key        : timestamp
    Both primary key and sort key are required in order to delete item.
    :param event:
    :return: Record gets deleted if the record found
    """
    data = json.loads(event['body'])

    if not required_field_present(data):
        return bad_response(MANDATORY_FIELD_MISSING)

    table.delete_item(
        Key={
            sort_key: data[sort_key],
            primary_key: data[primary_key]
        }
    )

    return {
        "statusCode": 204,
        "body": "Record deleted successfully"
    }


def required_field_present(data):
    return primary_key in data and sort_key in data


def bad_response(msg):
    return {
        "statusCode": 400,
        "body": msg
    }
