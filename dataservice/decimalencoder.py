import decimal
import json


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return int(o) if float(o) == int(o) else float(o)
        return super(DecimalEncoder, self).default(o)
